var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var appRouter = require('./routes/app')
var app = express();


var MongoClient = require('mongodb').MongoClient;
var db;


require('dotenv').config({ path: './process.env' });


MongoClient.connect(process.env.MONGO_URI, { useNewUrlParser: true, poolSize: 10  }).then(client => {
  
    var collection
    if(process.env.PRODUCTION == 'false') collection = client.db("Challenge");
    
    collection.collection("Users").findOne({},async (err,done) => {
      if(err) throw err
      else{
        console.log("DB TEST OK")
        app.db = collection
        await generateServer()
      }
    })
  }).catch(error => {throw error});

  async function generateServer(){
    app.use(logger('dev'));
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,jwt,Authorization");
      res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE');
      next();
    });

    app.use(express.static('./dist/frontend'));

     
 
    app.use('/', indexRouter);
    app.use('/App', appRouter);

    // serve angular front end files from root path
    app.use('/', express.static('app', { redirect: false }));
    
    // rewrite virtual urls to angular app to enable refreshing of internal pages
    app.get('*', function (req, res, next) {
        res.sendFile(path.resolve('dist/frontend/index.html'));
    });

  
  }


module.exports = app;
