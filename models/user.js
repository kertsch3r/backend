
function returnCurrentDate(){

    var now = new Date(); 
    var now_utc =  Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
    (now.getUTCHours() - 3), now.getUTCMinutes(), now.getUTCSeconds());
  
    return new Date(now_utc);
  
  }


class User {
    constructor(){
        this.userName = null
        this.userEmail = null
        this.userEnterpriseId = null
        this.userCreationDate = returnCurrentDate()
    }


    parseJSONData(json,type){

        if(json.userName != null){
            this.userName = json.userName
        }else  throw {
            code:406,
            message:"Missing user name"
        }

        if(json.userEmail != null){
            this.userEmail = json.userEmail
        }else throw {
            code:406,
            message:"Missing user email"
        }


       

      
    }
}

module.exports = User