var crypto = require('crypto');
const uuidv4 = require('uuid/v4');

require('dotenv').config({ path: './process.env' });

function returnCurrentDate(){

    var now = new Date(); 
    var now_utc =  Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
    (now.getUTCHours() - 3), now.getUTCMinutes(), now.getUTCSeconds());
  
    return new Date(now_utc);
  
  }

  
class PasswordRequest {

    constructor(){
        this.passwordRequestDate = returnCurrentDate()
        this.passwordRequestUserId = null 
        this.passwordRequestEnterpriseId = null 
        this.passwordRequestUserType = null
    }

    
    generateSave(){

        return new Promise((resolve,reject) => {

            crypto.randomBytes(20,(err,buffer) => {
                let token = buffer.toString('hex');
    
                this.passwordRequestLink = process.env.WEBSITE_URL+'NewPassword?solId='+token+'&source='+this.passwordRequestUserType
                this.passwordRequestToken = token
    
                let expDate = this.passwordRequestDate 
                    expDate = expDate.setHours(expDate.getHours() + 24)
                this.passwordRequestExpireDate = expDate
    
            
                 resolve(this) 
            })

        })      
    }
}

module.exports = PasswordRequest