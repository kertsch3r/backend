const ObjectId = require('mongodb').ObjectID;

class Embassy {

    constructor(){
        this.embassyName
        this.embassySegmentTags
        this.embassyAmbassorId
        this.embassyAddress
        this.embassyMembers = []
        this.embassyMeetingWeekDay
        this.embassyMeetingHour
    }

    parseJSONData(json){
        this.embassyName = json.embassyName
        this.embassySegmentTags = json.embassySegmentTags
        this.embassyAmbassorId = json.embassyAmbassorId
        this.embassyAddress = json.embassyAddress
        this.embassyMembers = json.embassyMembers
        this.embassyMeetingWeekDay = json.embassyMeetingWeekDay
        this.embassyMeetingHour = json.embassyMeetingHour    
    }
    
    validateObject(){
        for(let prop in this ) {
            if(this[prop] == null){ //if null check for wich prop
                return {     
                    message : `${prop} está nulo`,
                    valid:false
                }
            }
        }

        return {
            message : `Embassy is OK`,
            valid:true
        }
    }

}

module.exports = Embassy