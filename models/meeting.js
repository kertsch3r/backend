class Meeting {
    constructor(){
        this._id
        this.meetingEmbassyId
        this.meetingDate
        this.meetingHour
        this.meetingMembers = []
    }

    parseJSONData(json){
        this._id = json._id != null ? new ObjectId(json._id) : new ObjectId()
        this.meetingEmbassyId = this.meetingEmbassyId
        this.meetingDate = json.meetingDate
        this.meetingHour = json.meetingHour
        this.meetingMembers = json.meetingMembers
    }

    validateObject(){
        for(let prop in this ) {
            if(this[prop] == null){ //if null check for wich prop
                return {     
                    message : `${prop} está nulo`,
                    valid:false
                }
            }
        }

        return {
            message : `Meeting is OK`,
            valid:true
        }
    }

    addMeetingMember(id){
        this.meetingMembers.push({
            userId:id,
            status:"PENDING_ACCEPTANCE"
        })
    }

    
    removeMeetingMember(id){
        let index = this.meetingMembers.findIndex(x => x.userId == id)
        if(index != -1){
            this.meetingMembers.splice(index,1)
        }else return 'Usuário não encontrado'
    }


    setStatusToMeetingMember(id,status){
        let index = this.meetingMembers.findIndex(x => x.userId == id)
        if(index != -1){
            this.meetingMembers[index].status = status
        }else return 'Usuário não encontrado'
    }


}


module.exports = Meeting