var imageAPI = require('./image')
var Embassy = require('../models/embassy')
const ObjectId = require('mongodb').ObjectID;

module.exports.listEmbassies = (req) => {
    return new Promise((resolve,reject) => {
        req.app.db.collection('Embassies').find().toArray((err,embassies) => {
            if(err)reject(err)
            else{
                resolve(embassies)
            }
        })
    })
}

module.exports.listEmbassiesByUser = (req) => {
    return new Promise((resolve,reject) => {
        req.app.db.collection('Embassies').find({embassyMembers:req.params.id}).toArray((err,userEmbassies) => {
            if(err)reject(err)
            else{
                resolve(userEmbassies)
            }
        })
    })
}

module.exports.getEmbassyMembers = (req) => {
    return new Promise((resolve,reject) => {
        req.app.db.collection('Embassies').findOne({_id:new ObjectId(req.params.id)},(err,embassy) => {
            if(err)reject(err)
            else if(!embassy)reject({
                code:406,
                message:"Embaixada não encontrada"
            })
            else{
                let userIdArray = embassy.embassyMembers.map(x => new ObjectId(x))

                req.app.db.collection('Users').find({_id:{$in:userIdArray}}).toArray((err,users) => {
                    if(err)reject(err)
                    else{   

                        users = users.map(x => {
                            let userId = x._id.toString()

                            if(embassy.embassyAmbassorId == userId){
                                x.function = "LEADER"
                            }else{
                                x.function = "COMMON_USER"
                            }


                            return { 
                                id:x._id,
                                name:x.userName,
                                image:x.userImage,
                                function:x.function
                            }



                        })


                        let returnObject  = {
                            embassyName:embassy.embassyName,
                            embassyIconImage:embassy.embassyIconImage,
                            members:users
                        }
                        resolve(returnObject)
                    }
                })
            }
        })
    })
}

module.exports.getEmbassyById = (req) => {
    return new Promise((resolve,reject) => {
        req.app.db.collection('Embassies').findOne({_id:new ObjectId(req.params.id)},(err,embassy) => {
            if(err)reject(err)
            else if(!embassy)reject({
                code:406,
                message:"Embaixada não encontrada"
            })
            else{
                req.app.db.collection('Users').findOne({_id:new ObjectId(embassy.embassyAmbassorId)},(err,user) => {
                    if(err)reject(err)
                    if(!user)reject({
                        code:406,
                        message:"Embaixador não encontrado"
                    })
                    else{
                        embassy.embassyAmbassor = {
                            
                            name:user.userName,
                            phone:user.userPhone
                        }   

                        resolve(embassy)
                    }
                })
                
            }
        })
    })
}

module.exports.insertEmbassy = (req) => {
    return new Promise((resolve,reject) => {
        let imageReq = {
            body:{
                image:req.body.iconImageB64
            }
        }

        imageAPI.uploadImage(imageReq).then(imageURL => {
            let emb = new Embassy()
                emb.parseJSONData(req.body)

                emb.embassyIconImage = imageURL
            let resValidate = emb.validateObject()
            if(resValidate.valid == true) {
                req.app.db.collection('Embassies').insertOne(emb,(err,docsInserted) => {
                    if(err)reject(err)
                    else{
                        resolve({
                            message:"Embaixada criada com sucesso!"
                        })
                    }
                })  
            }else{
                reject({
                    code:406,
                    message:resValidate.message
                })
            }
           
        }).catch(err => reject(err))
       
    })
}

module.exports.updateEmbassy = (req) => {
    return new Promise((resolve,reject) => {

        req.app.db.collection('Embassies').findOne({embassyAmbassorId:req.params.id},(err,embassy) => {
            if(err)reject(err)
            else{

                let emb = new Embassy()
                emb.parseJSONData(req.body)

                if(req.body.updatedIconImage == true){
                    let imageReq = {
                        body:{
                            image:req.body.iconImageB64
                        }
                    }

                    imageAPI.uploadImage(imageReq).then(imageURL => {
                        emb.embassyIconImage = imageURL
                        let resValidate = emb.validateObject()


                        if(resValidate.valid == true) {
                            req.app.db.collection('Embassies').updateOne({_id:new ObjectId(req.params.id)},{$set:emb},(err,updated) => {
                                if(err)reject(err)
                                else if(updated.modifiedCount > 0) resolve({message:'Embaixada Atualizada'})
                                else reject({code:500,message:'Ocorreu um erro ao tentar atualizar a embaixada'})
                            })
                        }else{
                            reject({
                                code:406,
                                message:resValidate.message
                            })
                        }
                        

                    }).catch(err => reject(err))
                }else{
                    let resValidate = emb.validateObject()

                    if(resValidate.valid == true) {
                        req.app.db.collection('Embassies').updateOne({_id:new ObjectId(req.params.id)},{$set:emb},(err,updated) => {
                            if(err)reject(err)
                            else if(updated.modifiedCount > 0) resolve({message:'Embaixada Atualizada'})
                            else reject({code:500,message:'Ocorreu um erro ao tentar atualizar a embaixada'})
                        })
                    }else{
                        reject({
                            code:406,
                            message:resValidate.message
                        })
                    }

                }

            }
        })
    })
}

module.exports.removeEmbassy = (req) => {
    return new Promise((resolve,reject) => {
        req.app.db.collection('Embassies').deleteOne({_id:new ObjectId(req.params.id)},(err,items) => {
            if(err)resolve(err)
            resolve({message:'Embaixada removida'})
        })
    })
}