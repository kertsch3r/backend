const ObjectId = require('mongodb').ObjectID;

let embassyAPI = require('./embassy')
module.exports.getUserById = (req) => {
    return new Promise((resolve,reject) => {
        req.app.db.collection('Users').findOne({_id:new ObjectId(req.params.id)},(err,user) => {
            if(err)reject(err)
            else if(!user) reject({
                code:406,
                message:"Usuário não encontrado"
            })
            else{

                req.app.db.collection('Embassies').find({embassyAmbassorId:req.params.id}).count((err,count) => {
                    if(err)reject(err)
                    else{
                        if(count > 0) user.function = "Embaixador"
                        else user.function = "USUÁRIO"

                        resolve(user)
                    }   
                })
              
            }
        })
    })
}