var imageAPI = require('./image')
var Meeting = require('../models/meeting')

module.exports.listMeetings = (req) => {
    return new Promise((resolve,reject) => {
        req.app.db.collection('Meetings').find().toArray((err,Meetings) => {
            if(err)reject(err)
            else{
                resolve(Meetings)
            }
        })
    })
}

module.exports.listMeetingsByEmbassy = (req) => {
    return new Promise((resolve,reject) => {
        req.app.db.collection('Meetings').find({meetingEmbassyId:req.params.id}).toArray((err,embassyMeetings) => {
            if(err)reject(err)
            else{
                resolve(embassyMeetings)
            }
        })
    })
}

module.exports.getMeetingById = (req) => {
    return new Promise((resolve,reject) => {
        req.app.db.collection('Meetings').findOne({_id:new ObjectId(req.params.id)}).toArray((err,Meeting) => {
            if(err)reject(err)
            else if(!Meeting)reject({
                code:406,
                message:"Reunião não encontrada"
            })
            else{
                req.app.db.collection('Users').findOne({_id:new ObjectId(Meeting.MeetingAmbassorId)}).toArray((err,user) => {
                    if(err)reject(err)
                    if(!user)reject({
                        code:406,
                        message:"Embaixador não encontrado"
                    })
                    else{
                        Meeting.MeetingAmbassor = {
                            name:user.userName,
                            phone:user.userPhone
                        }   

                        resolve(Meeting)
                    }
                })
                resolve(Meetings)
            }
        })
    })
}

module.exports.insertMeeting = (req) => {
    return new Promise((resolve,reject) => {
        let imageReq = {
            body:{
                image:req.body.iconImageB64
            }
        }

        imageAPI.uploadImage(imageReq).then(imageURL => {
            let emb = new Meeting()
                emb.parseJSONData(req.body)

                emb.MeetingIconImage = imageURL
            let resValidate = emb.validateObject()
            if(resValidate.valid == true) {
                req.app.db.collection('Meetings').insertOne(req.body).toArray((err,docsInserted) => {
                    if(err)reject(err)
                    else{
                        resolve({
                            message:"Reunião criada com sucesso!"
                        })
                    }
                })  
            }else{
                reject({
                    code:406,
                    message:resValidate.message
                })
            }
           
        }).catch(err => reject(err))
       
    })
}

module.exports.updateMeeting = (req) => {
    return new Promise((resolve,reject) => {

        req.app.db.collection('Meetings').find({MeetingAmbassorId:req.params.id}).toArray((err,userMeetings) => {
            if(err)reject(err)
            else{

                let emb = new Meeting()
                emb.parseJSONData(req.body)

                if(req.body.updatedIconImage == true){
                    let imageReq = {
                        body:{
                            image:req.body.iconImageB64
                        }
                    }

                    imageAPI.uploadImage(imageReq).then(imageURL => {
                        emb.MeetingIconImage = imageURL
                        let resValidate = emb.validateObject()


                        if(resValidate.valid == true) {
                            req.app.db.collection('Content').updateOne({_id:emb._id},{$set:emb},(err,updated) => {
                                if(err)reject(err)
                                else if(updated.modifiedCount > 0) resolve({message:'Reunião Atualizada'})
                                else reject({code:500,message:'Ocorreu um erro ao tentar atualizar a Reunião'})
                            })
                        }else{
                            reject({
                                code:406,
                                message:resValidate.message
                            })
                        }
                        

                    }).catch(err => reject(err))
                }else{
                    let resValidate = emb.validateObject()

                    if(resValidate.valid == true) {
                        req.app.db.collection('Content').updateOne({_id:emb._id},{$set:emb},(err,updated) => {
                            if(err)reject(err)
                            else if(updated.modifiedCount > 0) resolve({message:'Reunião Atualizada'})
                            else reject({code:500,message:'Ocorreu um erro ao tentar atualizar a Reunião'})
                        })
                    }else{
                        reject({
                            code:406,
                            message:resValidate.message
                        })
                    }

                }

            }
        })
    })
}

module.exports.removeMeeting = (req) => {
    return new Promise((resolve,reject) => {
        req.app.db.collection('Meetings').deleteOne({_id:new ObjectId(req.params.id)},(err,items) => {
            if(err)resolve(err)
            resolve({message:'Reunião removida'})
        })
    })
}