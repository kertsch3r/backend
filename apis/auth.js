var jwt    = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var PasswordRequest = require('../models/passwordRequest')
var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');

const ObjectId = require('mongodb').ObjectID;


  require('dotenv').config({ path: './process.env' });
function returnCurrentDate(){

    var now = new Date(); 
    var now_utc =  Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
    (now.getUTCHours() - 3), now.getUTCMinutes(), now.getUTCSeconds());
  
    return new Date(now_utc);
  
  }
module.exports.validateToken = (req) => { //Converted
    return new Promise((resolve,reject) => {

      console.log(req.headers.jwt.includes("USER_GUEST") )
      if(req.headers.jwt.includes("USER_GUEST") != true ){
        jwt.verify(req.headers.jwt,process.env.JWT_SECRET, function(err, decoded) {
          if(err) {
              reject({
                  code:403,
                  message:"Token invalido, por favor faça login novamente"
              })
          }else{
              let sessionId = decoded.sessionId

              req.app.db.collection('Sessions').findOne({_id:new ObjectId(sessionId)},(err,session) => {
                  if(err) {
                      reject("Um erro ocorreu ao recuperar a sessão")
                  }
                  else {
                      if(session){
                          req.app.db.collection('Users').findOne({_id:new ObjectId(session.sessionUserId)},(err,user) => {
                              if(err){
                                  reject(err)
                              }
                              else {
                                  if(user){
                                      user.isGuest = false
                                      resolve(user)
                                  }else{
                                      reject({
                                          code:406,
                                          message:"Usuario nao encontrado"
                                      })
                                  }   
                              }         
                          })
                      }else {
                          reject({code:406,message:"O token fornecido nao está relacionado a nenhuma sessão vigente, por favor faça login novamente"})
                      }
                  }
              })
          }
        });
      }else{
        resolve({
          _id:"GUEST_USER",
          userName:"",
          isGuest:true,
          userEmail:"guest@kerzeit.com"
        })
      } 
        
    })
}

module.exports.validateTokenWEB = (req) => { //Converted
    return new Promise((resolve,reject) => {
        jwt.verify(req.headers.jwt,process.env.JWT_SECRET, function(err, decoded) {
            if(err) {
                reject({
                    code:403,
                    message:"Token invalido, por favor faça login novamente"
                })
            }else{
                let sessionId = decoded.sessionId

                req.app.db.collection('SessionsWeb').findOne({_id:new ObjectId(sessionId)},(err,session) => {
                    if(err) {
                        reject("Um erro ocorreu ao recuperar a sessão")
                    }
                    else {
                        if(session){
                            req.app.db.collection('UsersWeb').findOne({_id:new ObjectId(session.sessionUserId)},(err,user) => {
                                if(err){
                                    reject(err)
                                }
                                else {
                                    if(user){
                                        resolve(user)
                                    }else{
                                        reject({
                                            code:406,
                                            message:"Usuario nao encontrado"
                                        })
                                    }   
                                }         
                            })
                        }else {
                            reject({code:406,message:"O token fornecido nao está relacionado a nenhuma sessão vigente, por favor faça login novamente"})
                        }
                    }
                })
            }
          });
    })
}

module.exports.authenticateWEB = (req) => { //Converted
    return new Promise((resolve,reject) => {
        var authInfo = Buffer.from(req.headers.authorization.split(' ')[1], 'base64').toString().split(':')

        var user = authInfo[0]
        var pwd = authInfo[1]


        req.app.db.collection('UsersWeb').findOne({userEmail:user},function (error, user) {
            if (error) reject(error) ;
            else if(user){
                  bcrypt.compare(pwd, user.userHash, function(err, res) {
                    if(res == true){

                         var session = {
                            sessionUserEmail : user.userEmail,
                            sessionUserName : user.userName,
                            sessionUserId : user._id.toString(),
                            sessionEnterpriseId : user.userEnterpriseId,
                            sessionStartDate : returnCurrentDate(),
                            //sessionCoordinates:req.body.coordinates,
                            sessionEndDate : null
                         }
                          
                         req.app.db.collection('SessionsWeb').insertOne(session,(err,docsInserted) => {
                            if(err) reject(err)
                            let payload = {
                                 name: user.userName,
                                 email:user.userEmail,
                                 sessionId: docsInserted.insertedId.toString()
                             }
                             var token = jwt.sign(payload, process.env.JWT_SECRET,  { expiresIn: "10h"});

                             resolve({token:token,name:user.userName,email:user.userEmail})
                         })
                    }else{
                         reject({code:403,message:"Senha incorreta."})
                    }
                 });   
            }else{
                reject({code:403,message:"Usuario nao encontrado",email:user})
            }
          });
    })
}

module.exports.authenticate = (req) => { //Converted
    return new Promise((resolve,reject) => {
        var authInfo = Buffer.from(req.headers.authorization.split(' ')[1], 'base64').toString().split(':')

        var user = authInfo[0]
        var pwd = authInfo[1]


        req.app.db.collection('Users').findOne({userEmail:user},function (error, user) {
            if (error) reject(error) ;
            else if(user){
                  bcrypt.compare(pwd, user.userHash, function(err, res) {
                    if(res == true){

                         var session = {
                            sessionUserEmail : user.userEmail,
                            sessionUserName : user.userName,
                            sessionUserId : user._id.toString(),
                            sessionEnterpriseId : user.userEnterpriseId,
                            sessionStartDate : returnCurrentDate(),
                            //sessionCoordinates:req.body.coordinates,
                            sessionEndDate : null
                         }
                          
                         req.app.db.collection('Sessions').insertOne(session,(err,docsInserted) => {
                            if(err) reject(err)
                            let payload = {
                                 name: user.userName,
                                 email:user.userEmail,
                                 sessionId: docsInserted.insertedId.toString()
                             }
                             var token = jwt.sign(payload, process.env.JWT_SECRET,  { expiresIn: "10h"});

                             resolve({token:token,name:user.userName,email:user.userEmail})


                             
                         })
                    }else{
                         reject({code:403,message:"Senha incorreta."})
                    }
                 });   
            }else{
                reject({code:403,message:"Usuario nao encontrado",email:user})
            }
          });
    })
}

  module.exports.generateUserAction = (req,payload) => {
    return new Promise((resolve,reject) => {
        req.app.db.collection('UserActions').insertOne(payload,(err,done) => {
          if(err)reject(err)
          else resolve()
        })
    })
  }

  


  