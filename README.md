# Desafio EmbaixadaGV

Este repositório contém o backend e uma versão compilada do front end desenvolvido em angular 7 para o desafio proposto. Este projeto está consumindo um cluster de banco de dados não relacional gratuito na MongoDB Atlas.

# Instalação

	Clone o repositório
	rode npm install e aguarde o comando terminar
	rode npm start e acesse em seu navegador a url http://localhost:3000
	
	
# Layout 

	O Layout foi desenvolvido com base no projeto marvel localizado em https://marvelapp.com/6d55f7b/screen/53665309/handoff.
	
# Features

 Foram desenvolvidas as seguintes telas : 
 	
	Tela de listagem de embaixadas
	Tela de Visualização de embaixada
	Tela de Visualização de membros da embaixada,
	Tela de Visualização de Usuários
	
 No Backend foram desenvolvidos os seguintes métodos:
 	
	CRUD completo de embaixadas 
		POST -> /App/Embassy -> Insere uma embaixada
		GET -> /App/Embassy -> Lista todas as embaixadas
		PUT -> /App/Embassy/:id -> Atualiza uma embaixada (:id representa o id do banco de dados do item a ser atualizado)
		DELETE -> /App/Embassy/:id -> Remove uma embaixada (:id representa o id do banco de dados do item a ser removido)
		GET -> /App/Embassy/ById/:id -> pega todas  as informações de uma embaixada (:id representa o id do banco de dados da embaixada)
		GET -> /App/Embassy/ByUser/:id -> pega todas  as embaixadas relacionadas a um usuário (:id representa o id do banco de dados do usuário a ser consultado)
		GET -> /App/Embassy/Members/:id -> pega todas  as informações dos membros de uma embaixada(:id representa o id do banco de dados da embaixada a ser consultada)
		
	Conceito de Autenticação
		Esse método não possui uma url porém se encontra no caminho : Raiz/Apis/Auth 

