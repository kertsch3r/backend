var express = require('express');
var router = express.Router();
var api = require('../../apis/user')

function returnCurrentDate(){

    var now = new Date(); 
    var now_utc =  Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
    (now.getUTCHours() - 3), now.getUTCMinutes(), now.getUTCSeconds());
  
    return new Date(now_utc);
  
  }

  router.get('/ById/:id', function(req, res, next) {
    api.getUserById(req).then(dones => {
      res.send(dones)
    }).catch(err => {
        if(err.code != null){ 
          res.status(err.code).send(err.message)
        }else{
          res.status(500).send(err)
        }
    })  
}); 

module.exports = router