var express = require('express');
var router = express.Router();
var api = require('../../apis/meeting')

function returnCurrentDate(){

    var now = new Date(); 
    var now_utc =  Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
    (now.getUTCHours() - 3), now.getUTCMinutes(), now.getUTCSeconds());
  
    return new Date(now_utc);
  
  }


  
router.get('/', function(req, res, next) {
    api.listMeetings(req).then(dones => {
     res.send(dones)
    }).catch(err => {
        if(err.code != null){ 
          res.status(err.code).send(err.message)
        }else{
          res.status(500).send(err)
        }
    })  
}); 

router.get('/ByEmbassy/:id', function(req, res, next) {
  api.listMeetingsByEmbassy(req).then(dones => {
   res.send(dones)
  }).catch(err => {
      if(err.code != null){ 
        res.status(err.code).send(err.message)
      }else{
        res.status(500).send(err)
      }
  })  
});


router.get('/ById/:id', function(req, res, next) {
  api.getMeetingById(req).then(dones => {
   res.send(dones)
  }).catch(err => {
      if(err.code != null){ 
        res.status(err.code).send(err.message)
      }else{
        res.status(500).send(err)
      }
  })  
});

router.put('/:id', function(req, res, next) {
  api.updateMeeting(req).then(dones => {
   res.send(dones)
  }).catch(err => {
      if(err.code != null){ 
        res.status(err.code).send(err.message)
      }else{
        res.status(500).send(err)
      }
  })  
});

router.delete('/:id', function(req, res, next) {
  api.removeMeeting(req).then(dones => {
   res.send(dones)
  }).catch(err => {
      if(err.code != null){ 
        res.status(err.code).send(err.message)
      }else{
        res.status(500).send(err)
      }
  })  
});


router.post('/', function(req, res, next) {
  api.insertMeeting(req).then(dones => {
   res.send(dones)
  }).catch(err => {
      if(err.code != null){ 
        res.status(err.code).send(err.message)
      }else{
        res.status(500).send(err)
      }
  })  
});

module.exports = router;
