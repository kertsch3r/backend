var express = require('express');
var router = express.Router();
var api = require('../../apis/embassy')

function returnCurrentDate(){

    var now = new Date(); 
    var now_utc =  Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
    (now.getUTCHours() - 3), now.getUTCMinutes(), now.getUTCSeconds());
  
    return new Date(now_utc);
  
  }


  
router.get('/', function(req, res, next) {
    api.listEmbassies(req).then(dones => {
     res.send(dones)
    }).catch(err => {
        if(err.code != null){ 
          res.status(err.code).send(err.message)
        }else{
          res.status(500).send(err)
        }
    })  
}); 

router.get('/ByUser/:id', function(req, res, next) {
  api.listEmbassiesByUser(req).then(dones => {
   res.send(dones)
  }).catch(err => {
      if(err.code != null){ 
        res.status(err.code).send(err.message)
      }else{
        res.status(500).send(err)
      }
  })  
});


router.get('/Members/:id', function(req, res, next) {
  api.getEmbassyMembers(req).then(dones => {
   res.send(dones)
  }).catch(err => {
      if(err.code != null){ 
        res.status(err.code).send(err.message)
      }else{
        res.status(500).send(err)
      }
  })  
});




router.get('/ById/:id', function(req, res, next) {
  api.getEmbassyById(req).then(dones => {
   res.send(dones)
  }).catch(err => {
      if(err.code != null){ 
        res.status(err.code).send(err.message)
      }else{
        res.status(500).send(err)
      }
  })  
});

router.put('/:id', function(req, res, next) {
  api.updateEmbassy(req).then(dones => {
   res.send(dones)
  }).catch(err => {
      if(err.code != null){ 
        res.status(err.code).send(err.message)
      }else{
        res.status(500).send(err)
      }
  })  
});

router.delete('/:id', function(req, res, next) {
  api.removeEmbassy(req).then(dones => {
   res.send(dones)
  }).catch(err => {
      if(err.code != null){ 
        res.status(err.code).send(err.message)
      }else{
        res.status(500).send(err)
      }
  })  
});


router.post('/', function(req, res, next) {
  api.insertEmbassy(req).then(dones => {
   res.send(dones)
  }).catch(err => {
      if(err.code != null){ 
        res.status(err.code).send(err.message)
      }else{
        res.status(500).send(err)
      }
  })  
});

module.exports = router;
