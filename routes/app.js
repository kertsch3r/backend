var express = require('express');
var router = express.Router();
var auth = require('../apis/auth')


var embassyRouter = require('./childRoutes/embassy')
var userRouter = require('./childRoutes/user')


// router.use('/',(req,res,next) => {
//     auth.validateToken(req).then(user => {
//         req.user = user 
//         if(user.isGuest == true){ 
//             req.origin = "GUEST_USER"
//         }else{
//             req.origin = "USER" 
//         }
//         next()
//     }).catch(err => {
//         if(err.code) {
//             res.status(err.code).send(err.message)
//         }else{
//             res.status(500).send(err)
//         }
//     })
// });


router.use('/Embassy',embassyRouter)
router.use('/User',userRouter)




module.exports = router;
